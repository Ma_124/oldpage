#!/usr/bin/python3
import build.build as build

build.main(__name__, __file__)


class Config(build.Config):
    out = 'public'
    languages = ['haml']
    preserve_paths = False
